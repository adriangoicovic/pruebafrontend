import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class DataTableService{

    //Se genera la clase donde se llamará a los servicios que poblen las tablas

    public urlArray: string;
    public urlDict: string;

    constructor(
        private _http: HttpClient
    ){
        // Se definen las urls de los servicios donde se irán a buscar los datos
        this.urlArray = "http://patovega.com/prueba_frontend/array.php";    
        this.urlDict = "http://patovega.com/prueba_frontend/dict.php";
    }
    
    getArray():Observable<any>{
        // Metodo que obtiene el arreglo de los numeros.
        return this._http.get(this.urlArray);
    }
    
    getArrayDict():Observable<any>{
        // Metodo que obtiene el arreglo de los parrafos.
        return this._http.get(this.urlDict);
    }
}