import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataTableDictComponent } from './data-table-dict.component';

describe('DataTableDictComponent', () => {
  let component: DataTableDictComponent;
  let fixture: ComponentFixture<DataTableDictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataTableDictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableDictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
