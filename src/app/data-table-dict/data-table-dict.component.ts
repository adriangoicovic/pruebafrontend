import { Component, OnInit } from '@angular/core';
import { DataTableService } from '../services/data-table.service'
import { Observable } from 'rxjs';
import { Parrafo } from '../models/parrafo';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-data-table-dict',
  templateUrl: './data-table-dict.component.html',
  styleUrls: ['./data-table-dict.component.css'],
  providers: [DataTableService]
})
export class DataTableDictComponent implements OnInit {
  public parrafos: Array<Parrafo>;


  constructor(
    private _dataTableService: DataTableService
  ) {

  }

  ngOnInit(): void {
    this.generarTabla();
  }
  generarTabla(): void {
    
    this._dataTableService.getArrayDict().subscribe(
      // Se llama al metodo que consume el web service para obtener los datos
      response => {
        this.parrafos = new  Array<Parrafo>();
        let arrayParagrphs = JSON.parse(response.data); // Se obtiene el arreglo con los parrafos
        for (let i = 0; i < arrayParagrphs.length; i++) {
          let parrafoLeido = arrayParagrphs[i].paragraph.toUpperCase(); // Se convierten a mayusculas para facilitar la lectura
          let parrafo = new Parrafo();
          parrafo.indiceIncremental = i;
          
          //  Se recorre cada uno de los caracteres para contabilizarlos, se suma uno a cada aparicion de los caracteres
          for (let j = 0; j < parrafoLeido.length; j++) {
            switch (parrafoLeido[j]) {
              case 'A':
                parrafo.a++;
                break;
              case 'B':
                parrafo.b++;
                break;
              case 'C':
                parrafo.c++;
                break;
              case 'D':
                parrafo.d++;
                break;
              case 'E':
                parrafo.e++;
                break;
              case 'F':
                parrafo.f++;
                break;
              case 'G':
                parrafo.g++;
                break;
              case 'H':
                parrafo.h++;
                break;
              case 'I':
                parrafo.i++;
                break;
              case 'J':
                parrafo.j++;
                break;
              case 'K':
                parrafo.k++;
                break;
              case 'L':
                parrafo.l++;
                break;
              case 'M':
                parrafo.m++;
                break;
              case 'N':
                parrafo.n++;
                break;
              case 'O':
                parrafo.o++;
                break;
              case 'P':
                parrafo.p++;
                break;
              case 'Q':
                parrafo.q++;
                break;
              case 'R':
                parrafo.r++;
                break;
              case 'S':
                parrafo.s++;
                break;
              case 'T':
                parrafo.t++;
                break;
              case 'U':
                parrafo.u++;
                break;
              case 'V':
                parrafo.v++;
                break;
              case 'W':
                parrafo.w++;
                break;
              case 'X':
                parrafo.x++;
                break;
              case 'Y':
                parrafo.y++;
                break;
              case 'Z':
                parrafo.z++;
                break;

            }

          }
          
          this.parrafos.push(parrafo);
          // Se guarda el objeto parrafo con la contabilidad de los caracteres de cada parrafo.
          var numbersFromTexto = (parrafoLeido.replace(/[^0-9]/g, ' ')).split(' '); // Se extraen todos los numeros y se separan en un arreglo por parrafos
          let acum = 0;
          for(let j=0;j< numbersFromTexto.length;j++){
            if(numbersFromTexto[j]!="")
              acum += parseInt(numbersFromTexto[j]);
          }
          // Por cada numero que aparece se van sumando en el acumulador.
          parrafo.sumaNumerosContenidos = acum;

        }

      },
      error => {
        // Si existe algun error en la conexion con el servicio se muestra una alerta avisando el problema.
        console.log(error);
        Swal.fire({
          title: 'Error!',
          text: 'Ocurrió un error al obtener los datos',
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    );
  }

}
