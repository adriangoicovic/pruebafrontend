import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { DataTableComponent } from './data-table/data-table.component';
import { DataTableDictComponent } from './data-table-dict/data-table-dict.component';
import { routing, appRoutingProviders } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    DataTableComponent,
    DataTableDictComponent
  ],
  imports: [
    BrowserModule, FormsModule
    , HttpClientModule, routing],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
