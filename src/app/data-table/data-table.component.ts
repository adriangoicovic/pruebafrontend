import { Component, Input, OnInit, EventEmitter } from '@angular/core';
import { Numero } from '../models/numero';
import { DataTableService } from '../services/data-table.service'
import { Observable } from 'rxjs';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
  providers:[DataTableService]
})

export class DataTableComponent implements OnInit {
  
  public numbers: Array<Numero>;
  public dataReceived: Array<number>;
  private httpResponse: Observable<any>;
  @Input() arraySorted: string;


  constructor(private _dataTableService :DataTableService) {
  }
  

  ngOnInit(): void {
    this.generarTabla();
  }


  generarTabla():void{
    
    this._dataTableService.getArray().subscribe(
      // Se llama al metodo que consume el web service para obtener los datos
      response =>{
        this.numbers = new Array<Numero>();
        this.dataReceived = response.data;
        
        for (let i = 0; i < this.dataReceived.length; i++) {
          
          var numberInArray = this.numbers.some(u => u.numeroProcesado == this.dataReceived[i]);
          // Se recorre el arreglo recibido y se verifica por cada uno si ya se encuentra en el arrreglo 
          if (!numberInArray) { 
            let numero = new Numero(); // Si el numero no se encuentra en el arreglo resultado, se genera un objeto nuevo en el arreglo
            numero.numeroProcesado = this.dataReceived[i];  // Se setea el numero procesado
            numero.indiceIncremental = i;   // Se setea el indice de la aparicion del numero 
            numero.firstPosition = i;   // Se setea el indice de la primera aparicion 
            this.numbers.push(numero);  
          }
          else {
            // Si el numero se encuentra se obtiene el objeto que lo proceso 
            let index = this.numbers.findIndex(x => x.numeroProcesado === this.dataReceived[i]);
    
            this.numbers[index].quantity++; // Se le suma uno a la cantidad de apariciones.
            this.numbers[index].lastPosition = i; // Se setea el atributo de la ultima posicion en que se encontro el numero.
          }
        }
    
        
        // Se recorre el arreglo con los datos recibidos para ordenarlos con el algoritmo burbuja.
        for (let i = 0; i < this.dataReceived.length; i++) {
          for (let j = 0; j < this.dataReceived.length - 1 - i; j++) {
    
            if (this.dataReceived[j] > this.dataReceived[j + 1]) {
              var tmp = this.dataReceived[j + 1];
              this.dataReceived[j + 1] = this.dataReceived[j];
              this.dataReceived[j] = tmp;
            }
          }
        }
        this.arraySorted = this.dataReceived.join(','); // Se unen todos los numeros con una coma para mostrarlos en el input.
      },
      error =>{
        // Si existe algun error en la conexion con el servicio se muestra una alerta avisando el problema.
        console.log(error);
        Swal.fire({
          title: 'Error!',
          text: 'Ocurrió un error al obtener los datos',
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    );
  }


}




