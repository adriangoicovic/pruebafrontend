export class Parrafo {
    public indiceIncremental: number;
    public a: number;
    public b : number;
    public c : number;
    public d : number;
    public e: number;
    public f : number;
    public g : number;
    public h : number;
    public i: number;
    public j : number;
    public k : number;
    public l : number;
    public m: number;
    public n : number;
    public o : number;
    public p : number;
    public q: number;
    public r : number;
    public s : number;
    public t : number;
    public u: number;
    public v : number;
    public w : number;
    public x : number;
    public y : number;
    public z : number;
    public sumaNumerosContenidos : number;
        
    constructor() {
        this.indiceIncremental = 0;
        this.a = 0;
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = 0;
        this.p = 0;
        this.q = 0;
        this.r = 0;
        this.s = 0;
        this.t = 0;
        this.u = 0;
        this.v = 0;
        this.w = 0;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.sumaNumerosContenidos = 0;
     
    }
}