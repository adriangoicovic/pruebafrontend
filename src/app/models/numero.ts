export class Numero {
    public indiceIncremental: number;
    public numeroProcesado: number;
    public quantity : number;
    public firstPosition : number;
    public lastPosition : number;

    constructor() {
        this.indiceIncremental = 0;
        this.numeroProcesado = 0;
        this.quantity = 1;
        this.firstPosition = 0;
        this.lastPosition = 0;
    }
}