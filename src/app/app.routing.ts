import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { DataTableComponent } from './data-table/data-table.component';
import { DataTableDictComponent } from './data-table-dict/data-table-dict.component';


const appRoutes: Routes = [
    { path: '', component: DataTableComponent },
    { path: 'array', component: DataTableComponent },
    { path: 'dict', component: DataTableDictComponent }
];


export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);